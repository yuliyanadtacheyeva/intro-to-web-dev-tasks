const boxing = () => `<section class="boxing_section container">
<div class="img"></div>
<div class="boxing_info">
  <h3 class="section_subtitle">Whatever you get in the box</h3>
  <div class="boxing_specs">
    <div class="spec">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="" />
      5A Charger
    </div>
    <div class="spec">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="" />
      Extra battery
    </div>
    <div class="spec">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="" />
      Sophisticated bag
    </div>
    <div class="spec">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="" />
      User manual guide
    </div>
  </div>
</div>
</section>`;

export default boxing;
