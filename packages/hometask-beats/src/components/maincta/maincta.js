const maincta = () => `<section class="mainCta container">
    <div class="section_info">
    <h3 class="section_subtitle">Subscribe</h3>
    <div class="section_text">
        Lorem ipsum dolor sit amet, consectetur
    </div>
    </div>
    <div class="cta-btn">
    <input
        type="email"
        name="email"
        placeholder="Enter Your email address"
    />
    <button class="btn btn-red">Subscribe</button>
    </div>
    </section>`;

export default maincta;
