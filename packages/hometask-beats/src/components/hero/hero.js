const hero = () => `
<section>
<div class="wrapper-red">
<div class="hero_section container">
          <div class="hero_img"></div>
          <div class="hero_info">
            <h3 class="hero_subtitle section__subtitle">Hear it. Feel it</h3>
            <h1 class="hero_title">Move with the music</h1>
            <div class="price">
              <div class="price-new">$ 435</div>
              <div class="price-old">$ 465</div>
            </div>
            <button class="btn btn-white">BUY NOW</button>
          </div>
          </div>
          </div>
        </section>`;

export default hero;
