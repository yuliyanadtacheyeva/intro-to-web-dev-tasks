const feature = () => `<section class="feature_section container">
    <div class="feature_info">
    <h3 class="section_subtitle">
        Good headphones and loud music is all you need
    </h3>
    <div class="feature_specs">
        <div class="feature_specs-item feature_specs-1">
        <div class="feature_specs_img">
            <div class="feature_specs_outer">
            <div class="feature_specs_inner">
                <img
                src="./src/assets/icons/battery.svg"
                alt="battery"
                width="34"
                height="26"
                />
            </div>
            </div>
        </div>
        <div class="feature_specs_info">
            <h4>Battery</h4>
            <div class="section_text feature_text">
            Battery 6.2V-AAC codec
            </div>
            <a class="feature_specs_link" href="#">Learn more</a>
        </div>
        </div>

        <div class="feature_specs-item feature_specs-2">
        <div class="feature_specs_img">
            <div class="feature_specs_outer">
            <div class="feature_specs_inner">
                <img
                src="./src/assets/icons/bluetooth.svg"
                alt="battery"
                width="34"
                height="26"
                />
            </div>
            </div>
        </div>
        <div class="feature_specs_info">
            <h4>Bluetooth</h4>
            <div class="section_text feature_text">
            Battery 6.2V-AAC codec
            </div>
            <a class="feature_specs_link" href="#">Learn more</a>
        </div>
        </div>

        <div class="feature_specs-item feature_specs-3">
        <div class="feature_specs_img">
            <div class="feature_specs_outer">
            <div class="feature_specs_inner">
                <img
                src="./src/assets/icons/microphone.svg"
                alt="battery"
                width="33"
                height="33"
                />
            </div>
            </div>
        </div>
        <div class="feature_specs_info">
            <h4>Microphone</h4>
            <div class="section_text feature_text">
            Battery 6.2V-AAC codec
            </div>
            <a class="feature_specs_link" href="#">Learn more</a>
        </div>
        </div>
    </div>
    </div>
    <div class="feature_img"></div>
    </section>`;

export default feature;
