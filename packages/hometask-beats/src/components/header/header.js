const header = () => `
<header>
<div class="wrapper-red">
  <div class="header container">
    <div class="logo">
      <a href="#"
        ><img
          src="./src/assets/icons/logo.svg"
          alt="logo"
          width="73"
          height="66"
      /></a>
    </div>

    <div class="menu mobile-hidden">
      <div class="menu-search">
        <a href="#"
          ><img
            src="./src/assets/icons/search.svg"
            alt="logo"
            width="18"
            height="18"
        /></a>
      </div>
      <div class="menu-box">
        <a href="#"
          ><img
            src="./src/assets/icons/box.svg"
            alt="logo"
            width="18"
            height="20"
        /></a>
      </div>
      <div class="menu-user">
        <a href="#"
          ><img
            src="./src/assets/icons/user.svg"
            alt="logo"
            width="16"
            height="18"
        /></a>
      </div>
    </div>

    <div class="burger-menu">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
</div>
</header>
`;

export default header;
