const product = () => `<section class="product_section container">
    <div class="product_info">
    <h3 class="section_subtitle product_subtitle">Our Latest Product</h3>
    <div class="section_text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
        facilisis nunc ipsum aliquam, ante.
    </div>
    </div>

    <div class="product_cards_container">
    <div class="product-1 mobile-hidden">
        <div class="product_photo">
        <img
            src="./src/assets/images/product-01.png"
            alt=""
            width="227"
            height="306"
        />
        <div class="product_background"></div>
        </div>
        <div class="product_rate">
        <div class="product_rate_stars">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
        </div>
        <div class="product_rate_num">4.50</div>
        </div>
        <div class="product_selling">
        <div class="product_name">Red Headphone</div>
        <div class="product_price">$ 256</div>
        </div>
    </div>

    <div class="product-2">
        <div class="product_photo">
        <img
            src="./src/assets/images/product-02.png"
            alt=""
            width="227"
            height="306"
        />
        <div class="product_background"></div>
        </div>
        <div class="product_rate">
        <div class="product_rate_stars">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
        </div>
        <div class="product_rate_num">4.50</div>
        </div>
        <div class="product_selling">
        <div class="product_name">Blue Headphone</div>
        <div class="product_price">$ 235</div>
        </div>
    </div>

    <div class="product-3 mobile-hidden">
        <div class="product_photo">
        <img
            src="./src/assets/images/product-03.png"
            alt=""
            width="227"
            height="306"
        />
        <div class="product_background"></div>
        </div>
        <div class="product_rate">
        <div class="product_rate_stars">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
        </div>
        <div class="product_rate_num">4.50</div>
        </div>
        <div class="product_selling">
        <div class="product_name">Green Headphone</div>
        <div class="product_price">$ 245</div>
        </div>
    </div>
    </div>
    </section>`;

export default product;
