const clour = () => `<section class="clour_section container">
<h3 class="section_subtitle">Our Latest<br />colour collection 2021</h3>

<div class="clour_slider_container">
  <div class="clour_slider">
    <img
      src="./src/assets/images/clour-02.png"
      alt="Red headphones"
      class="clour-slide slide-2 mobile-hidden"
    />
    <img
      src="./src/assets/images/clour-01.png"
      alt="Blue headphones"
      class="clour-slide slide-1"
    />
    <img
      src="./src/assets/images/clour-03.png"
      alt="Orange headphones"
      class="clour-slide slide-3 mobile-hidden"
    />
  </div>
  <div class="clour_slider_controls">
    <img
      class="arrow-left"
      src="./src/assets/icons/arrow-left.svg"
      alt="arrow-left"
      width="15"
      height="30"
    />
    <img
      class="arrow-right"
      src="./src/assets/icons/arrow-right.svg"
      alt="arrow-right"
      width="15"
      height="30"
    />
  </div>
</div>
</section>`;

export default clour;
