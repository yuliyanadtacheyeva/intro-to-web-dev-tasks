import header from "./components/header/header.js";
import hero from "./components/hero/hero.js";
import boxing from "./components/boxing/boxing.js";
import clour from "./components/clour/clour.js";
import feature from "./components/feature/feature.js";
import footer from "./components/footer/footer.js";
import maincta from "./components/maincta/maincta.js";
import product from "./components/product/product.js";


const app = document.querySelector("#app");

const render = () => {
  app.innerHTML = `
        ${header()}
        ${hero()}
        ${clour()}
        ${feature()}
        ${product()}    
        ${boxing()}
        ${maincta()}
        ${footer()}
    `;
};

render();
