const feature = () => (
  `<section class="section-feature">
    <div class="section-feature-left">
      <h1 class="title">Discover new music</h1>

      <div class="section-feature-btns">
        <button class="btn">Charts</button>
        <button class="btn">Songs</button>
        <button class="btn">Artists</button>
      </div>

      <div class="feature__text">
        By joining you can benefit by listening to the latest albums released
      </div>
    </div>

    <div class="feature__img">
      <img
        src="src/assets/images/music-titles.png"
        alt="feature image"
        height="512"
        width="512"
      />
    </div>
  </section>`
)

export default feature;
