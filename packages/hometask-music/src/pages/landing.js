const landing = () => (
  `<section class="section-landing">
    <h1 class="title">Feel the music</h1>

    <div class="landing__text">Stream over 10 million songs with one click</div>

    <button class="btn join__link">Join now</button>
  </section>`
)

export default landing;
