const HEADER_LIST = ["Discover", "Join", "Sign in"];

export const landingHeader = `<nav>
  <ul class="header__nav-menu">
      <li><a class= "discover__link-landing">${HEADER_LIST[0]}</a></li>
      <li><a class= "join__link">${HEADER_LIST[1]}</a></li>
      <li><a class= "sign__link">${HEADER_LIST[2]}</a></li>
    </ul>
</nav>`;

export const signUpHeader = `<nav>
  <ul class="header__nav-menu">
    <li><a class= "discover__link-signUp">${HEADER_LIST[0]}</a></li>
    <li><a class= "sign__link">${HEADER_LIST[2]}</a></li>
  </ul>
</nav>`;

export const featureHeader = `<nav>
  <ul class="header__nav-menu">
    <li><a class= "join__link">${HEADER_LIST[1]}</a></li>
    <li><a class= "sign__link">${HEADER_LIST[2]}</a></li>
  </ul>
</nav>`;
