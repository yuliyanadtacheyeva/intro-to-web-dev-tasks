const footer = () => (
  `<footer>
    <nav>
      <ul class="footer__nav-menu">
        <li><a href="#">About As</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="#">CR Info</a></li>
        <li><a href="#">Twitter</a></li>
        <li><a href="#">Facebook</a></li>
      </ul>
    </nav>
  </footer>`
);

export default footer;
