const main = page => (`
  <main>
    ${ page() }
  </main>
`)

export default main;
