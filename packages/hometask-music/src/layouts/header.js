const header = navigation => (
  `<header>
    <div class="header__logo">
      <a>
        <img class="header__logo__img" src="src/assets/icons/logo.svg" alt="logo" />
        <div class="header__logo__text">Simo</div>
      </a>
    </div>
    ${ navigation }
  </header>`
);

export default header;
