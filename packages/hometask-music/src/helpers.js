const handleLandingClass = app => {
  app.classList.remove("signUp");
  app.classList.add("landing");
};

const handleLandingListeners = handler => {
  const joinLink = document.querySelectorAll(".join__link");

  Array.prototype.forEach.call(joinLink, link =>
    link.addEventListener("click", handler)
  );

  const discoverLinkFromLanding = document.querySelector(
    ".discover__link-landing"
  );
  discoverLinkFromLanding.addEventListener("click", handler);
};


const handleFeatureClass = app => {
  app.classList.remove("landing");
  app.classList.remove("signUp");
};

const handleListeners = (className, handler) => {
  document.querySelector(className).addEventListener("click", handler);
};

const handleSignUpClass = app => {
  app.classList.toggle("landing");
  app.classList.toggle("signUp");
};

export const handleRenderLanding = (app, handler) => {
  handleLandingClass(app);

  handleLandingListeners(handler);
};

export const handleRenderFeature = (app, handler) => {
  handleFeatureClass(app);

  handleListeners(".join__link", handler);
};

export const handleRenderSignUp = (app, handler) => {
  handleSignUpClass(app);

  handleListeners(".discover__link-signUp", handler);
};
