import header from "./src/layouts/header";
import footer from "./src/layouts/footer";
import main from "./src/layouts/main";
import feature from "./src/pages/feature";
import signUp from "./src/pages/signUp";
import landing from "./src/pages/landing";

import { featureHeader, landingHeader, signUpHeader } from "./src/constants";
import {
  handleRenderLanding,
  handleRenderFeature,
  handleRenderSignUp,
} from "./src/helpers";

const app = document.querySelector("#App");

app.classList.add("container", "landing");

const render = (navigation = landingHeader, page = landing) => {
  app.innerHTML = `
    ${ header(navigation) }
    ${ main(page) }
    ${ footer() }
  `;
};

render();

const updateContent = e => {
  const targetTitle = e.target.innerText;
  const targetClass = e.target.className;

  if (targetClass === 'discover__link-landing') {
    render(featureHeader, feature);
    handleRenderFeature(app, updateContent);
  }

  if (targetClass === 'discover__link-signUp') {
    render(landingHeader, landing);
    handleRenderLanding(app, updateContent);
  }

  if (targetTitle === 'Join' || targetTitle === 'Join now') {
    render(signUpHeader, signUp);
    handleRenderSignUp(app, updateContent);
  }
}

handleRenderLanding(app, updateContent);
